from django.shortcuts import render
from django.views.generic import View,TemplateView,ListView
from .models import *
from .forms import *


'''View home'''
def home(request):
    return render(request,'index.html')


'''View menu'''
def menu(request):
    return render(request,'menu/menu.html')


'''View menu de usuario logueado'''
def menuLogin(request):
    return render(request,'menu/menu_login.html')


''' View Curso'''
def listadoCurso(request):
    modulo1 = Modulo1.objects.filter(estado=True)
    modulo2 = Modulo2.objects.filter(estado=True)
    modulo3 = Modulo3.objects.filter(estado=True)
    modulo4 = Modulo4.objects.filter(estado=True)
    modulo5 = Modulo5.objects.filter(estado=True)
    return render(request,'curso/curso.html',
        {
            'modulo1':modulo1,
            'modulo2':modulo2,
            'modulo3':modulo3,
            'modulo4':modulo4,
            'modulo5':modulo5
        }
    )

'''Modulo1'''

def listadoModulo1(request):
    modulos = Modulo1.objects.filter(estado=True)
    return render(request,'curso/modulos/modulo1.html',{'modulos':modulos})

def listadoTema1(request,id):
    tema = Modulo1.objects.get(id=id)
    return render(request,'curso/temas/temas_modulo1.html',{'tema':tema})

def listadoPractica1(request,id):
    practica = Modulo1.objects.get(id=id)
    return render(request,'curso/practicas/practicas_modulo1.html',{'practica':practica})




''' Modulo2 '''
def listadoModulo2(request):
    modulos = Modulo2.objects.filter(estado=True)
    return render(request,'curso/modulos/modulo2.html',{'modulos':modulos})

def listadoTema2(request,id):
    tema = Modulo2.objects.get(id=id)
    return render(request,'curso/temas/temas_modulo2.html',{'tema':tema})

def listadoPractica2(request,id):
    practica = Modulo2.objects.get(id=id)
    return render(request,'curso/practicas/practicas_modulo2.html',{'practica':practica})




''' Modulo3 '''
def listadoModulo3(request):
    modulos = Modulo3.objects.filter(estado=True)
    return render(request,'curso/modulos/modulo3.html',{'modulos':modulos})

def listadoTema3(request,id):
    tema = Modulo3.objects.get(id=id)
    return render(request,'curso/temas/temas_modulo3.html',{'tema':tema})

def listadoPractica3(request,id):
    practica = Modulo3.objects.get(id=id)
    return render(request,'curso/practicas/practicas_modulo3.html',{'practica':practica})






''' Modulo4 '''
def listadoModulo4(request):
    modulos = Modulo4.objects.filter(estado=True)
    return render(request,'curso/modulos/modulo4.html',{'modulos':modulos})

def listadoTema4(request,id):
    tema = Modulo4.objects.get(id=id)
    return render(request,'curso/temas/temas_modulo4.html',{'tema':tema})

def listadoPractica4(request,id):
    practica = Modulo4.objects.get(id=id)
    return render(request,'curso/practicas/practicas_modulo4.html',{'practica':practica})





''' Modulo5 '''
def listadoModulo5(request):
    modulos = Modulo5.objects.filter(estado=True)
    return render(request,'curso/modulos/modulo5.html',{'modulos':modulos})

def listadoTema5(request,id):
    tema = Modulo5.objects.get(id=id)
    return render(request,'curso/temas/temas_modulo5.html',{'tema':tema})

def listadoPractica5(request,id):
    practica = Modulo5.objects.get(id=id)
    return render(request,'curso/practicas/practicas_modulo5.html',{'practica':practica})