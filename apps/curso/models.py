from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class Modulo1(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.IntegerField('Número', unique = True, blank = False, null = False)
    tema = models.CharField('Tema', max_length=150, blank=False, null=False)
    contenido_tema = RichTextUploadingField('Contenido',blank=True)
    practica = RichTextUploadingField('Práctica',blank=True)
    solucion = RichTextUploadingField('Solución',blank=True)
    estado = models.BooleanField('Estado', default= True)
    fecha_creacion = models.DateField(
        'Fecha de creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Módulo 1'
        verbose_name_plural = 'Módulo 1'
        ordering = ['numero']

    def __str__(self):
        return self.tema



class Modulo2(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.IntegerField('Número', unique = True, blank = False, null = False)
    tema = models.CharField('Tema', max_length=150, blank=False, null=False)
    contenido_tema = RichTextUploadingField('Contenido',blank=True)
    practica = RichTextUploadingField('Práctica',blank=True)
    solucion = RichTextUploadingField('Solución',blank=True)
    estado = models.BooleanField('Estado', default= True)
    fecha_creacion = models.DateField(
        'Fecha de creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Módulo 2'
        verbose_name_plural = 'Módulo 2'
        ordering = ['numero']

    def __str__(self):
        return self.tema



class Modulo3(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.IntegerField('Número', unique = True, blank = False, null = False)
    tema = models.CharField('Tema', max_length=150, blank=False, null=False)
    contenido_tema = RichTextUploadingField('Contenido',blank=True)
    practica = RichTextUploadingField('Práctica',blank=True)
    solucion = RichTextUploadingField('Solución',blank=True)
    estado = models.BooleanField('Estado', default= True)
    fecha_creacion = models.DateField(
        'Fecha de creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Módulo 3'
        verbose_name_plural = 'Módulo 3'
        ordering = ['numero']

    def __str__(self):
        return self.tema



class Modulo4(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.IntegerField('Número', unique = True, blank = False, null = False)
    tema = models.CharField('Tema', max_length=150, blank=False, null=False)
    contenido_tema = RichTextUploadingField('Contenido',blank=True)
    practica = RichTextUploadingField('Práctica',blank=True)
    solucion = RichTextUploadingField('Solución',blank=True)
    estado = models.BooleanField('Estado', default= True)
    fecha_creacion = models.DateField(
        'Fecha de creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Módulo 4'
        verbose_name_plural = 'Módulo 4'
        ordering = ['numero']

    def __str__(self):
        return self.tema



class Modulo5(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.IntegerField('Número', unique = True, blank = False, null = False)
    tema = models.CharField('Tema', max_length=150, blank=False, null=False)
    contenido_tema = RichTextUploadingField('Contenido',blank=True)
    practica = RichTextUploadingField('Práctica',blank=True)
    solucion = RichTextUploadingField('Solución',blank=True)
    estado = models.BooleanField('Estado', default= True)
    fecha_creacion = models.DateField(
        'Fecha de creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Módulo 5'
        verbose_name_plural = 'Módulo 5'
        ordering = ['numero']

    def __str__(self):
        return self.tema



    