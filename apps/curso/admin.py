from django.contrib import admin
from .models import *


class Modulo1Admin(admin.ModelAdmin):
    search_fields = ['tema']
    list_display = ('numero','tema','estado','fecha_creacion',)


class Modulo2Admin(admin.ModelAdmin):
    search_fields = ['tema']
    list_display = ('numero','tema','estado','fecha_creacion',)


class Modulo3Admin(admin.ModelAdmin):
    search_fields = ['tema']
    list_display = ('numero','tema','estado','fecha_creacion',)


class Modulo4Admin(admin.ModelAdmin):
    search_fields = ['tema']
    list_display = ('numero','tema','estado','fecha_creacion',)


class Modulo5Admin(admin.ModelAdmin):
    search_fields = ['tema']
    list_display = ('numero','tema','estado','fecha_creacion',)


admin.site.register(Modulo1,Modulo1Admin)
admin.site.register(Modulo2,Modulo2Admin)
admin.site.register(Modulo3,Modulo3Admin)
admin.site.register(Modulo4,Modulo4Admin)
admin.site.register(Modulo5,Modulo5Admin)
