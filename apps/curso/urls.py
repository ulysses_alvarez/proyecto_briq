from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import *

urlpatterns = [
    path('',login_required(listadoCurso),name='listado_curso'),
    
    path('modulo_1/',login_required(listadoModulo1),name='modulo_1'),
    path('tema_1/<int:id>/',login_required(listadoTema1),name='tema_1'),
    path('practica_1/<int:id>/',login_required(listadoPractica1),name='practica_1'),
    
    path('modulo_2/',login_required(listadoModulo2),name='modulo_2'),
    path('tema_2/<int:id>/',login_required(listadoTema2),name='tema_2'),
    path('practica_2/<int:id>/',login_required(listadoPractica2),name='practica_2'),

    path('modulo_3/',login_required(listadoModulo3),name='modulo_3'),
    path('tema_3/<int:id>/',login_required(listadoTema3),name='tema_3'),
    path('practica_3/<int:id>/',login_required(listadoPractica3),name='practica_3'),

    path('modulo_4/',login_required(listadoModulo4),name='modulo_4'),
    path('tema_4/<int:id>/',login_required(listadoTema4),name='tema_4'),
    path('practica_4/<int:id>/',login_required(listadoPractica4),name='practica_4'),

    path('modulo_5/',login_required(listadoModulo5),name='modulo_5'),
    path('tema_5/<int:id>/',login_required(listadoTema5),name='tema_5'),
    path('practica_5/<int:id>/',login_required(listadoPractica5),name='practica_5'),
]


