from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm


class LoginForm(AuthenticationForm):

    class Meta:
        model = User
        fields = ['username','password',]
        labels = {
            'username': 'usuario',
            'password': 'contraseña',
        }


class RegistroForm(UserCreationForm):
    
    class Meta:
        model = User
        fields = ['username','first_name','last_name','email',]
        labels = {
            'username': 'usuario',
            'first_name': 'Nombre(s)',
            'last_name': 'Apellidos',
            'email': 'Correo electrónico',
        }


    
    