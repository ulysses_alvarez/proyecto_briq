"""briq URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.decorators import login_required
from apps.curso.views import home, menu, menuLogin
from apps.usuarios.views import Login,logoutUsuario

from apps.curso import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('probabilidad_y_estadistica/',include(('apps.curso.urls','curso'))),
    path('usuarios/',include(('apps.usuarios.urls','usuarios'))),
    path('',home,name='index'),
    path('menu/',menu,name='menu'),
    path('menu_/',menuLogin,name='menu_login'),
    path('accounts/login/',Login.as_view(),name='login'),
    path('logout/',login_required(logoutUsuario),name='logout'),
    

    path('ckeditor',include('ckeditor_uploader.urls')),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
